#include <MKRWAN.h>
#include <Wire.h>
#include <cstdio>
#include <dht11.h>
#include <math.h>

const int MPU6050_addr=0x68;
int16_t AccX,AccY,AccZ,Temp,GyroX,GyroY,GyroZ;
double gX, gY, gZ, roll, pitch, sR, sP;
double rS[] = {0.0, 0.0, 0.0};
double pS[] = {0.0, 0.0, 0.0};
char s_buf[256], temp_char;
int p = 0;

LoRaModem m;
dht11 DHT11;

void setup(){
  Wire.begin();
  Wire.beginTransmission(MPU6050_addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  pinMode(7, OUTPUT);
  Serial.begin(9600);

  Wire.beginTransmission(MPU6050_addr);
  Wire.write(0x1C); 
  Wire.endTransmission();
  
  Wire.requestFrom(MPU6050_addr, 1);
  byte x = Wire.read(); 
  
  x = x | 0b00011000;     
  
  Wire.beginTransmission(MPU6050_addr);
  Wire.write(0x1C); 
  Wire.write(x);      
  Wire.endTransmission();
}

void loop(){
  long t = millis();
  DHT11.read(6);
  Wire.beginTransmission(MPU6050_addr);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU6050_addr, 14, true);
  
  AccX = Wire.read() << 8 | Wire.read();
  AccY = Wire.read() << 8 | Wire.read();
  AccZ = Wire.read() << 8 | Wire.read();
  Temp = Wire.read() << 8 | Wire.read();
  GyroX = Wire.read() << 8 | Wire.read();
  GyroY = Wire.read() << 8 | Wire.read();
  GyroZ = Wire.read() << 8 | Wire.read();
  
  gX = (AccX / 16384.0);
  gY = (AccY / 16384.0);
  gZ = (AccZ / 16384.0);
  roll = grav(gX, gY, gZ);
  pitch = grav(gY, gX, gZ);
  
  rS[p] = roll;
  pS[p] = pitch;
  sR = 0.0;
  sP = 0.0;
  for (int i = 0; i < 3; ++i) {
    sR += rS[i];
    sP += pS[i];
  }

  s_recv(t);
  s_send();
  
  if (++p == 3) p = 0;
}
 
double dist(double a, double b) {
  return sqrt(pow(a, 2) + pow(b, 2));
}

double grav(double r, double a, double b) {
  return atan2(r, dist(a, b)) * (180.0 / M_PI);
}

void s_send() {
  Serial.print("<"); 
  Serial.print(Temp / 340.00 + 36.53);
  Serial.print(","); 
  Serial.print(DHT11.humidity);
  
  Serial.print(","); 
  Serial.print(sR / 3.0);
  Serial.print(","); 
  Serial.print(sP / 3.0);
  Serial.print(",");
  Serial.print(s_buf);
  Serial.println(">");
}

void s_recv(int t) {
  while (!Serial.available()) { delay(5); }
  int i = 0;
  memset(&s_buf, 0, sizeof(s_buf));
  while (Serial.available() > 0) {
    temp_char = Serial.read();
    if (temp_char == '\n') break;
    s_buf[i++] = temp_char;
  }
}
